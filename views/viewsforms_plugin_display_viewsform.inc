<?php

/**
 * @file
 * Views display plugin viewsform.
 */

/**
 * The plugin that handles a viewsform display.
 * 
 * $form['viewsform'] = array(
 *   '#type' => 'viewsform',
 *   '#title' => t('My Viewform Field'),
 *   '#view' => array(
 *     'name' => 'name_of_view',
 *     'display' => 'name_of_viewsform_display',
 *     'arguments' => array(),
 *   ),
 * );
 *
 * @ingroup views_display_plugins
 */
class viewsforms_plugin_display_viewsform extends views_plugin_display {
  var $viewsform_processing;
  
  /**
   * 
   * none|before|after|both|override
   */
  var $viewsform_process = 'none';
  
  function option_definition () {
    $options = parent::option_definition();
    
    return $options;
  }

  /**
   * Return a FAPI array.
   * 
   * This method is called in viewsforms_process_viewsform() and used
   * to expand viewform elements. It allows plugins to further expand
   * the element.
   * 
   * @see viewsforms_process_viewsform()
   *
   * @return array Fapi array
   */
  function execute() {
    $this->viewsform_processing = TRUE;
    
    // $this->view->render()
    //   $view->execute();
    //   $view->style_plugin->pre_render($this->result);
    //   hook_views_pre_render($view);
    //   $view->field[$id]->pre_render($this->result);
    //   return $view->display_handler->render();
    $return = $this->view->render();
    $this->viewsform_processing = FALSE;
    
    return $return;
  }
  
  /**
   * Fully render the display for the purposes of a live preview or
   * some other AJAXy reason.
   */
  function preview() {
    return $this->view->render();
  }
  
  /**
   * Render this display.
   * 
   * @see theme_viewsform()
   */
  function render() {
    if ($this->viewsform_processing) {
      $this->viewsform_processing = FALSE;
      
      // Return nothing
      return;
    }else {
      // Default method of rendering a display.
      // Called in theme_viewsform().
      return parent::render();
    }
  }
  
  function uses_exposed() {
    $trace = debug_backtrace();
    if ($trace[1]['class'] == 'view' && $trace[1]['function'] == 'build') {
      // view:build calls view::render_exposed_form and we don't want that.
      // TODO patch for views.module so debug_backtrace() isn't needed.
      return FALSE;
    }else {
      return parent::uses_exposed();
    }
  }
  
  /**
   * Enter description here...
   *
   * @see template_preprocess_views_view()
   * 
   * @param array $element
   * @param array $form_state
   * @return array
   */
  function viewsform_process($element, $edit, &$form_state) {
    return $element;
  }
  

  
  function viewsform_theme($element) {
    
  }
}