<?php

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 */
class viewsforms_handler_field_formelement extends views_handler_field {

  function allow_advanced_render() {
    return FALSE;
  }
  
  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();
    
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  /**
   * Run before any fields are rendered.
   *
   * This gives the handlers some time to set up before any handler has
   * been rendered. In this case expanding the form element.
   *
   * @param $values
   *   An array of all objects returned from the query.
   */
  function pre_render($values) {
    
  }
  
  function render($values) {
    // Nothing to render.
    return '';
  }
  
  function viewsform_process($element, $form_state) {
    
    return $element;
  }
}