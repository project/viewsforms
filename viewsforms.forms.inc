<?php

/**
 * @file
 * FAPI handlers for the viewsform element.
 * 
 * @ingroup forms
 */

/**
 * Expand a viewsform element.
 *
 * @param array $element
 * @param mixed $edit
 * @param array $form_state
 * @param array $form
 * @return array
 *   FAPI element array
 * 
 * @ingroup forms
 * @see template_preprocess_views_view()
 *   A lot of code is copied from this function.
  * @see viewsforms_process_viewsform()
 *   Delegator function.
 */
function _viewsforms_process_viewsform(&$element, $edit, &$form_state, $form) {
  //echo '_viewsforms_process_viewsform<br/>';
  
  if (isset($element['#view']['name'])) {
    $name = $element['#view']['name'];
    if (isset($form_state['viewsform'][$name])) {
      // reuse view object in drupal_rebuild_form in multistate forms
      $view = &$form_state['viewsform'][$name];
    }elseif(isset($element['#view']['view'])) {
      // allow to use custom view objects
      $view = &$element['#view']['view'];
      $form_state['viewsform'][$name] = &$view;
    }else {
      $view = views_get_view($name);
      $form_state['viewsform'][$name] = &$view;
    }
  }elseif (isset($element['#view']['view'])) {
    // no name set: view could run twice in multistate forms
    $view = &$element['#view']['view'];
  }
  
  if (empty($view)) {
    return array(
      '#value' => t('View not found')
    );
  }
  
  if (isset($element['#view']['display'])) {
    // TODO make sure it is a viewsform display
    // or at least force to be a viewsform display.
    $view->set_display($element['#view']['display']);
  }
  
  if (isset($element['#view']['arguments'])) {
    $view->set_arguments($element['#view']['arguments']);
  }
  if (!empty($element['#value']['arguments'])) {
    $arguments = arg(NULL, $element['#value']['arguments']);
    $view->set_arguments($arguments);
  }

  // Add remembered filters
  $display_id = ($view->display_handler->is_defaulted('filters')) ? 'default' : $view->current_display;
  if (isset($_SESSION['views'][$view->name][$display_id])) {
    if (is_array($element['#value']['filters'])) {
      $element['#value']['filters'] = array_merge($_SESSION['views'][$view->name][$display_id], $element['#value']['filters']);
    }else {
      $element['#value']['filters'] = $_SESSION['views'][$view->name][$display_id];
    }
    form_set_value($element, $element['#value'], $form_state);
  }
  
  if (isset($element['#value']['filters'])) {
    // @see view::_build
    $view->set_exposed_input($element['#value']['filters']);
    
    // @see views_exposed_form_submit()
    $view->exposed_data = $element['#value']['filters'];
    $view->exposed_raw_input = $element['#value']['filters'];
  }
  
  // TODO pass filter input to attachments if they have 'inherit_exposed_filters' option set to TRUE

  // Execute display before expanding our form element.
  $display_output = $view->execute_display();
  $display = &$view->display_handler;

  // Pass view so it can be used in other element handlers (like theme_viewsform())
  $element['#view']['view'] = &$view;
  
  $viewsform_process = isset($display->viewsform_process) ? $display->viewsform_process : 'none';
  if (in_array($viewsform_process, array('before', 'both', 'override'))) {
    $element = $display->viewsform_process($element, $edit, $form_state);
    if ($viewsform_process == 'override') {
      return $element;
    }
  }
  
  $element['#tree'] = TRUE;
  if (!isset($element['#title'])) {
    $element['#title'] = filter_xss_admin($view->get_title());
  }
  if (!isset($element['#submit'])) {
    // default #submit value
    $element['#submit'] = array();
  }
  
  if ($view->use_ajax) {
    $element['#ahah'] = array(
      'path' => 'viewsforms/ajax',
      'wrapper' => $element['#id'] .'-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
      'progress' => array('type' => 'bar', 'message' => t('Please wait...')),
      'event' => 'viewsform_update'
    );
  }

  $element['arguments'] = array(
    '#type' => 'textfield',
    '#default_value' => $element['#value']['arguments']
  );
  $element['name'] = array(
    '#type' => 'hidden',
    '#value' => $view->name
  );
  $element['display_id'] = array(
    '#type' => 'hidden',
    '#value' => $view->current_display
  );
    
  if (!isset($element['header']) && (!empty($view->result) || $display->get_option('header_empty'))) {
    $element['header'] = array(
      '#value' => $display->render_header(),
    );
  }

  if ($display->uses_exposed()) {
    $element['filters'] = array(
      '#type' => 'viewsform_filters',
      '#default_value' => $element['#value']['filters'],
      '#view' => array(
        'view' => &$view
      ),
      '#submit' => $element['#submit'], // delegate submit handlers
    );
  }
    
  if (!empty($view->attachment_before)) {
    // TODO allow attachments to extend the form
    $element['attachment_before'] = array(
      '#value' => $view->attachment_before
    );
  }
    
  if (!empty($view->result) || !empty($view->style_plugin->definition['even empty'])) {
    $element['rows'] = array(
      '#type' => 'viewsform_style',
      '#default_value' => $element['#value']['rows'],
      '#view' => array(
        'view' => &$view
      ),
    );
  }else {
    $element['empty'] = array(
      '#value' => $display->render_empty()
    );
  }

  if (!empty($view->pager['use_pager'])) {
    $pager_type = ($view->pager['use_pager'] === 'mini' ? 'mini' : 'pager');
    $element['pager'] = array(
      '#type' => 'viewsform_pager',
      '#pager' => array(
        'limit' => $view->pager['items_per_page'],
        'element' => $view->pager['element'],
        'type' => $pager_type
      ),
      '#submit' => $element['#submit'], // delegate submit handlers
    );
  }
    
  if (!empty($view->attachment_after)) {
    // TODO allow attachments to extend the form
    $element['attachment_after'] = array(
      '#value' => $view->attachment_after
    );
  }
    
  if (!isset($element['footer']) && (!empty($view->result) || $display->get_option('footer_empty'))) {
    $element['footer'] = array(
      '#value' => $display->render_footer(),
    );
  }
  
  if (in_array($viewsform_process, array('after', 'both'))) {
    $element = $display->viewsform_process($element, $edit, $form_state);
  }
  return $element;
}

/**
 * Expand the filters part of a viewsform element.
 * 
 * @param array $element
 * @param mixed $edit
 * @param array $form_state
 * @return array
 *   FAPI element array
 * 
 * @ingroup forms
 * @see views::render_exposed_form()
 *   A lot of code is copied from this function.
 *   It is however different since we don't use
 *   views' form.inc.
 * @see views_exposed_form()
 *   A lot of code is copied from this function.
 * @see viewsforms_validate_viewsform_filters()
 *   Validate callback.
 * @see viewsforms_submit_viewsform_filters()
 *   Submit callback.
 */
function viewsforms_process_viewsform_filters(&$element, $edit, &$form_state) {
  //echo 'viewsforms_process_viewsform_filters<br/>';
  
  $view = &$element['#view']['view'];
  $display = &$view->display_handler->display;
  
  if (!isset($element['#theme'])) {
    // use views own theming functions by default
    $element['#theme'] = views_theme_functions('views_exposed_form', $view, $display);
  }

  // Some types of displays (eg. attachments) may wish to use the exposed
  // filters of their parent displays instead of showing an additional
  // exposed filter form for the attachment as well as that for the parent.
  if (!$view->display_handler->displays_exposed()) {
    // TODO make this work
    // no need to build this
    return array();
  }
  
  // Use a temporary $form_state so filters cannot change our $form_state
  $tmp_form_state = array(

  );

  // needed for views_process_dependency()
  if (!empty($view->ajax)) {
    $tmp_form_state['ajax'] = TRUE;
  }
  
  // Let form plugins know this is for exposed widgets.
  $tmp_form_state['exposed'] = TRUE;
  // Add #info so we can use views own theming functions
  $element['#info'] = array();
  
  // Go through each filter and let it generate its info.
  foreach ($view->filter as $id => $filter) {
    $view->filter[$id]->exposed_form($element, $tmp_form_state);
    if ($info = $view->filter[$id]->exposed_info()) {
      $element['#info']['filter-' . $id] = $info;
    }
  }

  array_unshift($element['#submit'], 'viewsforms_submit_viewsform_filters');
  $element['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#submit' => $element['#submit'],
  );

  // TODO make sure this works
  views_add_js('dependent');
  return $element;
}

/**
 * Validate handler for exposed filters
 *
 * @param array $element
 * @param array $form_state
 * 
 * @ingroup forms
 * @see views_exposed_form_validate
 *   A lot of code is copied from this function.
 */
function viewsforms_validate_viewsform_filters($element, &$form_state) {
  //echo 'viewsforms_viewsform_filters_validate<br/>';
  $view = &$element['#view']['view'];

  // Use a temporary $form_state so filters cannot change our $form_state
  $tmp_form_state = array(
    // TODO is this correct? same as $form_state['values'][...]['filters'] ?
    'values' => $element['#value'],
  );
  
  // TODO handlers could change $tmp_form_state or use form_error()
  foreach (array('field', 'filter') as $type) {
    $handlers = &$view->$type;
    foreach ($handlers as $key => $handler) {
      $handlers[$key]->exposed_validate($element, $tmp_form_state);
    }
  }
  
  // TODO merge changes of $tmp_form_state into $form_state?
}

/**
 * Submit handler for exposed filters in a viewsform element
 *
 * @param array $form
 * @param array $form_state
 * 
 * @ingroup forms
 * @see views_exposed_form_submit
 *   A lot of code is copied from this function.
 */
function viewsforms_submit_viewsform_filters($form, &$form_state) {
  echo 'viewsforms_submit_viewsform_filters<br/>';
  
  $parents = $form_state['clicked_button']['#parents'];
  array_pop($parents); // pop button so we get the viewsform_filters element
  $element = &$form;
  $values = &$form_state['values'];
  foreach ($parents as $name) {
    $element = &$element[$name];
    $values = &$values[$name];
  }
  
  $view = &$element['#view']['view'];

  // Use a temporary $form_state so filters cannot change our $form_state
  $tmp_form_state = array(
    'values' => $values
  );
  
  // TODO handlers could change $tmp_form_state or use form_error()
  foreach (array('field', 'filter') as $type) {
    $handlers = &$view->$type;
    foreach ($handlers as $key => $handler) {
      $handlers[$key]->exposed_submit($element, $tmp_form_state);
    }
  }
  
  // TODO merge changes of $tmp_form_state into $form_state?
  
  // Only use $form_state['rebuild'] when we're in a multistate form
  if (!empty($form_state['storage'])) {
    $form_state['rebuild'] = TRUE;
  }else {
    $form_state['redirect'] = FALSE;
  }
}

/**
 * Expand a viewsform_style element.
 *
 * This element is always a child of a viewsform element:
 * 
 *   $element == $form['myviewsform']['rows']
 * 
 * @param array $element
 * @param mixed $edit
 * @param array $form_state
 * @return array
 *   FAPI element array
 * 
 * @ingroup forms
 * @see views_plugin_style::render()
 *   A lot of code is copied from this function.
 * @see views_plugin_style::render_grouping()
 *   A lot of code is copied from this function.
 */
function viewsforms_process_viewsform_style(&$element, $edit, &$form_state) {
  $view = &$element['#view']['view'];
  $style_plugin = &$view->style_plugin;

  if (!isset($element['#theme'])) {
    // use views own theming functions by default
    $element['#theme'] = 'viewsform_style_content';
  }
  
  $viewsform_process = isset($style_plugin->viewsform_process) ? $style_plugin->viewsform_process : 'none';
  if (in_array($viewsform_process, array('before', 'both', 'override'))) {
    $element = $style_plugin->viewsform_process($element, $edit, $form_state);
    if ($viewsform_process == 'override') {
      return $element;
    }
  }

  $grouping_field = $style_plugin->options['grouping'];
  $use_grouping_field = ($grouping_field && isset($view->field[$grouping_field]));

  // Group the rows according to the grouping field, if specified.
  $sets = array();
  if ($use_grouping_field) {
    // We cannot use the value of grouping fields in the formstructure
    // since values could be non scalar or very long, therefor we give
    // every unique grouping field value a number.
    // Also it is possible that a row doesn't have the grouping field:
    // we use key 0 for these.
    $grouping_key = 1;
    $grouping_keys = array();
    
    foreach ($view->result as $row) {
      if (!isset($row->{$view->field[$grouping_field]->field_alias})) {
        $grouping = 0;
      }else {
        $grouping_field_value = $row->{$view->field[$grouping_field]->field_alias};
        if (!isset($grouping_keys[$grouping_field_value])) {
          $grouping_keys[$grouping_field_value] = $grouping_key++;
        }
        $grouping = $grouping_keys[$grouping_field_value];
      }

      $sets[$grouping][] = $row;
    }
  }
  else {
    // Create a single group with an empty grouping field.
    $sets[''] = $view->result;
  }

  foreach ($sets as $key => $records) {
    if ($use_grouping_field) {
      $row_element = &$element[$key];
    }else {
      $row_element = &$element;
    }

    foreach ($records as $i => $row) {
      if ($use_grouping_field && is_array($element['#value'])) {
        $default_value = $element['#value'][$key][$i];
      }else {
        $default_value = $element['#value'][$i];
      }
      
      // not all style plugins do use a row plugin, but we need a full form structure
      $row_element[$i] = array(
        '#type' => 'viewsform_row',
        '#default_value' => $default_value,
        '#view' => array(
          'view' => &$view,
          'row_values' => $row,
        ),
      );
    }
  }

  if (in_array($viewsform_process, array('after', 'both'))) {
    $element = $style_plugin->viewsform_process($element, $edit, $form_state);
  }
  return $element;
}

/**
 * FAPI #post_render callback for viewsform_style elements.
 *
 */
function viewsforms_post_render_viewsform_style($content, &$element) {
  
}


/**
 * Expand a viewsform_pager element.
 *
 * This element is always a child of a viewsform element:
 * 
 *   $element == $form['myviewsform']['pager']
 * 
 * @param array $element
 * @param mixed $edit
 * @param array $form_state
 * @return array
 *   FAPI element array
 * 
 * @ingroup forms
 * @see theme_pager
 *   A lot of code is copied from this function.
 * @see viewsforms_submit_viewsform_pager
 *   Submit callback.
 */
function viewsforms_process_viewsform_pager($element, $edit, &$form_state) {
  global $pager_page_array, $pager_total;
  
  $limit = isset($element['#pager']['limit']) ? $element['#pager']['limit'] : 10;
  $pager_element = isset($element['#pager']['element']) ? $element['#pager']['element'] : 0;
  // TODO can we use parameters? do we need to?
  $parameters = array();
  $quantity = isset($element['#pager']['quantity']) ? $element['#pager']['quantity'] : 9;
  $pager_type = isset($element['#pager']['type']) ? $element['#pager']['type'] : 'pager';

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$pager_element] + 1;
  // max is the maximum page number
  $pager_max = $pager_total[$pager_element];
  
  if ($pager_type == 'pager') {
    // first is the first page listed by this pager piece (re quantity)
    $pager_first = $pager_current - $pager_middle + 1;
    // last is the last page listed by this pager piece (re quantity)
    $pager_last = $pager_current + $quantity - $pager_middle;
    // End of marker calculations.
  
    // Prepare for generation loop.
    $i = $pager_first;
    if ($pager_last > $pager_max) {
      // Adjust "center" if at end of query.
      $i = $i + ($pager_max - $pager_last);
      $pager_last = $pager_max;
    }
    if ($i <= 0) {
      // Adjust "center" if at start of query.
      $pager_last = $pager_last + (1 - $i);
      $i = 1;
    }
    // End of generation loop preparation.
  }

  $has_previous = ($pager_page_array[$pager_element] > 0);
  $has_next = ($pager_page_array[$pager_element] < ($pager_total[$pager_element] - 1));

  // prepare #name of submit buttons
  $button_name = 'viewsform_pager_'.$pager_element.'_';
  
  // add our own submit handler
  array_unshift($element['#submit'], 'viewsforms_submit_viewsform_pager');

  if (!isset($element['#theme'])) {
    $element['#theme'] = 'viewsform_pager_content';
  }
  
  $element['current'] = array(
    '#type' => 'hidden',
    '#value' => $pager_current-1,
    '#name' => 'viewsform_pager['.$pager_element.']'
  );
  
  if ($pager_total[$pager_element] > 1) {
    if ($has_previous && $pager_type == 'pager') {
      $element[] = array(
        '#type' => 'submit',
        '#value' => t('first'),
        '#attributes' => array(
          'class' => 'pager-first',
        ),
        '#submit' => $element['#submit'],
        '#name' => $button_name . 0
      );
    }
    if ($has_previous) {
      $element[] = array(
        '#type' => 'submit',
        '#value' => t('previous'),
        '#attributes' => array(
          'class' => 'pager-previous',
        ),
        '#submit' => $element['#submit'],
        '#name' => $button_name . ($pager_current-2)
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max && $pager_type == 'pager') {
      if ($i > 1) {
        $element[] = array(
          '#type' => 'submit',
          '#disabled' => TRUE,
          '#value' => '�',
          '#attributes' => array(
            'class' => 'pager-ellipsis',
          ),
          '#submit' => $element['#submit']
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $element[] = array(
            '#type' => 'submit',
            '#value' => $i,
            '#attributes' => array(
              'class' => 'pager-item',
            ),
            '#submit' => $element['#submit'],
            '#name' => $button_name . ($i-1)
          );
        }
        if ($i == $pager_current) {
          $element[] = array(
            '#type' => 'submit',
            '#disabled' => TRUE,
            '#value' => $i,
            '#attributes' => array(
              'class' => 'pager-current',
            ),
            '#submit' => $element['#submit'],
            '#name' => $button_name . ($i-1)
          );
        }
        if ($i > $pager_current) {
          $element[] = array(
            '#type' => 'submit',
            '#value' => $i,
            '#attributes' => array(
              'class' => 'pager-item',
            ),
            '#submit' => $element['#submit'],
            '#name' => $button_name . ($i-1)
          );
        }
      }
      if ($i < $pager_max) {
        $element[] = array(
          '#type' => 'submit',
          '#disabled' => TRUE,
          '#value' => '�',
          '#attributes' => array(
            'class' => 'pager-ellipsis',
          ),
          '#submit' => $element['#submit']
        );
      }
    }else {
      $element[] = array(
        '#type' => 'submit',
        '#disabled' => TRUE,
        '#value' => $pager_current, // Don't change this value!!!
        '#attributes' => array(
          'class' => 'pager-current',
        ),
        '#submit' => $element['#submit'],
        '#name' => $button_name . ($pager_current-1)
      );
    }
    // End generation.
    
    if ($has_next) {
      $element[] = array(
        '#type' => 'submit',
        '#value' => t('next'),
        '#attributes' => array(
          'class' => 'pager-next',
        ),
        '#submit' => $element['#submit'],
        '#name' => $button_name . ($pager_current)
      );
    }
    if ($has_next && $pager_type == 'pager') {
      $element[] = array(
        '#type' => 'submit',
        '#value' => t('last'),
        '#attributes' => array(
          'class' => 'pager-last',
        ),
        '#name' => $button_name . ($pager_total[$pager_element] - 1)
      );
    }
  }
  
  return $element;
}

/**
 * Submit handler for pager buttons in a viewsform element
 * 
 * @param array $form
 * @param array $form_state
 * 
 * @ingroup forms
 * @see viewsforms_init()
 */
function viewsforms_submit_viewsform_pager($form, &$form_state) {
  echo 'viewsforms_submit_viewsform_pager<br/>';
  // Only use $form_state['rebuild'] when we're in a multistate form
  if (!empty($form_state['storage'])) {
    // We don't set values here; see viewsforms_init()
    $form_state['rebuild'] = TRUE;
  }else {
    $form_state['redirect'] = FALSE;
  }
}


/**
 * Expand a viewsform_row element.
 *
 * This element is always a child of a viewsform_style element:
 * 
 *   $element == $form['myviewsform']['rows'][$i]
 * 
 * Or when a grouping field is used:
 * 
 *   $element == $form['myviewsform']['rows'][$grouping_key][$i]
 * 
 * @param array $element
 * @param mixed $edit
 * @param array $form_state
 * @return array
 *   FAPI element array
 * 
 * @ingroup forms
 * @see template_preprocess_views_view_fields()
 *   A lot of code is copied from this function.
 */
function viewsforms_process_viewsform_row(&$element, $edit, &$form_state) {
  $view = &$element['#view']['view'];
  $style_plugin = &$view->style_plugin;
  $row = $element['#view']['row_values'];

  if ($style_plugin->uses_row_plugin() && !empty($style_plugin->row_plugin) && isset($style_plugin->row_plugin->viewsform_process)) {
    $viewsform_process = $style_plugin->row_plugin->viewsform_process;
  }else {
    $viewsform_process = 'none';
  }
  if (in_array($viewsform_process, array('before', 'both', 'override'))) {
    $element = $style_plugin->row_plugin->viewsform_process($element, $edit, $form_state);
    if ($viewsform_process == 'override') {
      return $element;
    }
  }

  // $form['myviewsform']['rows']
  foreach ($view->field as $id => $field) {
    // TODO what about excluded fields?
    // (we could use elements with #type value)
    if (empty($field->options['exclude'])) {
      $element[$id] = array(
        '#type' => 'viewsform_field',
        '#default_value' => $element['#value'][$id],
        '#view' => array(
          'view' => &$view,
          'field_handler' => &$view->field[$id],
          'row_values' => $row
        ),
      );
    }else {
      $element[$id] = array(
        '#type' => 'value',
        '#value' => $row->{$field->field_alias},
      );
    }
  }

  if (in_array($viewsform_process, array('after', 'both'))) {
    $element = $style_plugin->row_plugin->viewsform_process($element, $edit, $form_state);
  }
  return $element;
}

/**
 * Expand a viewsform_field element.
 *
 * This element is always a child of a viewsform_row element:
 * 
 *   $element == $form['myviewsform']['rows'][$i][$field_id]
 * 
 * Or when a grouping field is used:
 * 
 *   $element == $form['myviewsform']['rows'][$grouping_key][$i][$field_id]
 *
 * @param array $element
 * @param mixed $edit
 * @param array $form_state
 * @return array
 *   FAPI element array
 * 
 * @ingroup forms
 */
function viewsforms_process_viewsform_field(&$element, $edit, &$form_state) {
  $view = &$element['#view']['view'];
  $field = &$element['#view']['field_handler'];
  $row = $element['#view']['row_values'];

  $viewsform_process = isset($field->viewsform_process) ? $field->viewsform_process : 'none';
  if (in_array($viewsform_process, array('before', 'both', 'override'))) {
    $element = $field->viewsform_process($element, $edit, $form_state);
    if ($viewsform_process == 'override') {
      return $element;
    }
  }

  //$field_output = $field->theme($row);
  $element['#title'] = check_plain($field->label());

  if (isset($field->field_alias) && isset($row->{$field->field_alias})) {
    $element['#value'] = $row->{$field->field_alias};
  }else {
    $element['#value'] = NULL;
  }

  if (in_array($viewsform_process, array('after', 'both'))) {
    $element = $field->viewsform_process($element, $edit, $form_state);
  }
  return $element;
}



