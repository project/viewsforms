<?php

function example_viewsforms_example2($form_state) {
  //echo 'example_viewsforms_form1<br/>';

  if (empty($form_state['storage']['step'])) {
    // we are coming in without a step, so default to step 1
    $form_state['storage']['step'] = 1;
  }  
  
  switch ($form_state['storage']['step']) {
    case 1:
      $form['field1'] = array (
        '#type' => 'textfield',
        '#default_value' => example_viewsforms_dv($form_state['storage']['values'][1]['field1']),
        '#title' => t('Test Field'));
      
      $form['myviewsform'] = array (
        '#type' => 'viewsform',
        '#title' => t('Test Views Form Field'),
        '#view' => array(
          'name' => 'example_viewsform',
          'display' => 'viewsform_1',
          'arguments' => array(),
        ),
        '#default_value' => example_viewsforms_dv($form_state['storage']['values'][1]['myviewsform']),
        // Special submit handlers
        '#submit' => array(
          'example_viewsforms_example2_storage'
        ),
      );
      break;
    case 2:
      $form['field1'] = array (
        '#type' => 'textfield',
        '#default_value' => example_viewsforms_dv($form_state['storage']['values'][1]['field1']),
        '#title' => t('Test Field'));
      $form['field2'] = array (
        '#type' => 'textfield',
        '#default_value' => example_viewsforms_dv($form_state['storage']['values'][2]['field2']),
        '#title' => t('Test Field 2'));
      $form['test'] = array(
        '#value' => '<pre>'.print_r($form_state['storage']['values'][1], 1).'</pre>'
      );
      break;
    case 3:
      $form['field3'] = array (
        '#type' => 'textfield',
        '#default_value' => example_viewsforms_dv($form_state['storage']['values'][3]['field3']),
        '#title' => t('Test Field 3'));
      break;
    default:
      ///blah blah, really you should never get here
      break;
  }
    
  // don't show back button on first tab
  if ($form_state['storage']['step'] > 1) {
    $form['previous'] = array(
      '#type' => 'submit',
      '#value' => '<< Previous'
    );
  }
  // don't show back button on last tab
  if ($form_state['storage']['step'] < 3) {
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => 'Next >>'
    );
  }
  return $form;
}

/**
 * Submit callback that stores values of current step into $form_state['storage'].
 */
function example_viewsforms_example2_storage($form, &$form_state) {
  //echo 'example_viewsforms_example2_storage<br/>';
  
  //save the values for the current step into the storage array
  $form_state['storage']['values'][$form_state['storage']['step']] = $form_state['values'];
}

/**
 * Submit callback that handles the next/previous buttons.
 */
function example_viewsforms_example2_submit($form, &$form_state) {
  //echo 'example_viewsforms_example2_submit<br/>';
  
  //save the values for the current step into the storage array
  $form_state['storage']['values'][$form_state['storage']['step']] = $form_state['values'];
    
  // check the button that was clicked and action the step change
  if ($form_state['clicked_button']['#id']=='edit-previous') {
    $form_state['storage']['step']--;
  } elseif ($form_state['clicked_button']['#id']=='edit-next') {
    $form_state['storage']['step']++;
  }
    
  //tell Drupal we are redrawing the same form
  $form_state['rebuild'] = TRUE;  
}