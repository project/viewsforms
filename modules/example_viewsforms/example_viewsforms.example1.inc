<?php

function example_viewsforms_example1($form_state) {
  $form = array();
  
  $form['description'] = array(
    '#value' => 'Very simple form'
  );

  $form['field1'] = array (
    '#type' => 'textfield',
    '#title' => t('Test Field'),
    '#description' => 'The user-input for this field should be kept.',
  );
  
  $form['viewsform'] = array (
    '#type' => 'viewsform',
    '#title' => t('Test Field'),
    '#view' => array(
      'name' => 'example_viewsform',
      'display' => 'viewsform_1',
      'arguments' => array(),
    ),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit'
  );
  
  return $form;
}

/**
 * 
 */
function example_viewsforms_example1_submit($form, &$form_state) {
  $form_state['redirect'] = FALSE;
}