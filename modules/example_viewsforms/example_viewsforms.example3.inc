<?php

function example_viewsforms_example3($form_state) {
  $form = array();
  
  $form['description'] = array(
    '#value' => 'Very simple form'
  );

  $form['field1'] = array (
    '#type' => 'textfield',
    '#title' => t('Test Field'),
    '#description' => 'The user-input for this field should be kept.',
  );
  
  $view = views_get_view($element['#view']['name']);
  $view->set_display($element['#view']['display']);  
  $view->set_arguments($element['#view']['arguments']);
  
  $form['viewsform'] = array (
    '#type' => 'viewsform',
    '#title' => t('Test Field'),
    '#view' => array(
      'name' => 'example_viewsform',
      'display' => 'viewsform_1',
      'arguments' => array(),
    ),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit'
  );
  
  return $form;
}

/**
 * 
 */
function example_viewsforms_example3_submit($form, &$form_state) {
  $form_state['redirect'] = FALSE;
}