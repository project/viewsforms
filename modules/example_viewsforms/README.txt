
== Example Views Forms

This module provides some example of what can be accomplished with the
viewsforms module.

To try it out, rename the "example_viewsforms.info.disabled" file to 
"example_viewsforms.info".