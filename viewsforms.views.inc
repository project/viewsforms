<?php

/**
 * @file
 * Views plugin and handler definitions for the viewsforms module.
 */

/**
 * Implementation of hook_views_plugins().
 */
function viewsforms_views_plugins() {
  return array(
    'module' => 'viewsforms', // This just tells our themes are elsewhere.

    'display' => array(
      'viewsform' => array(
        'path' => drupal_get_path('module', 'viewsforms') .'/views',
        'title' => t('Form Element'),
        'help' => t('Display the view as a form element.'),
        'handler' => 'viewsforms_plugin_display_viewsform',
        'theme' => 'views_view',
        'use ajax' => TRUE,
        'use pager' => TRUE,
        'accept attachments' => TRUE,
        'help topic' => 'display-viewsform',
      ),
    ),
  );
}
