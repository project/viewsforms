<?php

/**
 * @file
 * Page callbacks that are registered in viewsforms_menu().
 */

/**
 * Menu callback to load a viewsform via AJAX.
 * 
 * @see views_ajax()
 * @see filefield.module filefield_js()
 */
function viewsforms_ahah($field_name) {
  $response = new stdClass();
  $response->status = FALSE;
  $response->data = '';
  
  if (empty($field_name) || empty($_POST['form_build_id'])) {
    // Invalid request.
    print drupal_to_js($response);
    exit;
  }

  // Build the new form.
  $form_state = array('submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);

  if (!$form) {
    // Invalid form_build_id.
    print drupal_to_js($response);
    exit;
  }

  // form_get_cache() doesn't yield the original $form_state, but form_builder() does.
  $built_form = $form;
  $built_form_state = $form_state;

  $built_form += array('#post' => $_POST);
  $built_form = form_builder($_POST['form_id'], $built_form, $built_form_state);

  // Clean ids, so that the same element doesn't get a different element id
  // when rendered once more further down.
  form_clean_id(NULL, TRUE);
  
  // TODO alter viewsform element?
  
  
  
  
  
  
  // Render the form for output.
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
  );
  drupal_alter('form', $form, array(), 'viewsforms_js');
  $form_state = array('submitted' => FALSE);
  $form = form_builder('viewsforms_js', $form, $built_form_state);
  // TODO get viewsform element
  $viewsform = $form[$field_name];

  // We add a div around the new content to tell AHAH to let this fade in.
  $viewsform['#prefix'] = '<div class="ahah-new-content">';
  $viewsform['#suffix'] = '</div>';

  // TODO seperate status_messages?
  $response->data = theme('status_messages') . drupal_render($viewsform);

  // Settings could've changed so send them again
  // TODO seperate setting?
  $javascript = drupal_add_js(NULL, NULL);
  if (isset($javascript['setting'])) {
    $response->data .= '<script type="text/javascript">jQuery.extend(Drupal.settings, '. drupal_to_js(call_user_func_array('array_merge_recursive', $javascript['setting'])) .');</script>';
    //$response->settings = array_merge_recursive($javascript['setting']);
  }

  $response->status = TRUE;
  
  $GLOBALS['devel_shutdown'] = false;
  print drupal_to_js($response);
  exit;
}