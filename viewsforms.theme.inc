<?php

/**
 * @file
 * Theme callbacks that are registered in viewsforms_theme().
 * 
 * @ingroup themeable
 */

/**
 * Format a viewsform element.
 *
 * @param $element
 *   An associative array containing the properties of the element.
 *   Properties used: title, value, options, description, required and attributes.
 * @return
 *   A themed HTML string representing the viewsform element.
 *
 * @ingroup themeable
 */
function theme_viewsform($element) {
  //unset($element['#view']['view']);
  //return '<pre>'.print_r($element, 1).'</pre>';
  //return $element['#children'];
  
  $view = &$element['#view']['view'];

  views_add_css('views');
  
  $element['#children'] = '<div id="'. $element['#id'] .'">'. $element['#children'] .'</div>';
  unset($element['#value']);
  return theme('fieldset', $element);
}

/**
 * Theme the contents of a viewsform element.
 *
 * @see template_preprocess_views_view()
 * 
 * @param $element
 *   An associative array containing the properties of the element.
 * @return
 *   A themed HTML string representing the viewsform element.
 *
 * @ingroup themeable
 */
function theme_viewsform_content($element) {
  $view = &$element['#view']['view'];

  

}

/**
 * Format a viewsform_filters element.
 *
 * @param $element
 *   An associative array containing the properties of the element.
 *   Properties used: title, value, options, description, required and attributes.
 * @return
 *   A themed HTML string representing the viewsform element.
 *
 * @ingroup themeable
 */
function theme_viewsform_filters($element) {
  return $element['#children'];
}

/**
 * Format a viewsform_style element.
 * 
 * @param $element
 *   An associative array containing the properties of the element.
 * @return
 *   A themed HTML string representing the viewsform element.
 *
 * @ingroup themeable
 */
function theme_viewsform_style($element) {
  return $element['#children'];
}

/**
 * Theme the content of a viewsform_style element.
 *
 * @see template_preprocess_views_view()
 * 
 * @param $element
 *   An associative array containing the properties of the element.
 * @return
 *   A themed HTML string representing the viewsform element.
 *
 * @ingroup themeable
 */
function theme_viewsform_style_content($element) {
  $view = &$element['#view']['view'];
  $style = &$view->style_plugin;
  
  // TODO this makes all form children not to be rendered
  // 
  return $style->render($view->result);
}

/**
 * Format a viewsform_pager element.
 * 
 * @param $element
 *   An associative array containing the properties of the element.
 * @return
 *   A themed HTML string representing the viewsform element.
 *
 * @ingroup themeable
 */
function theme_viewsform_pager($element) {
  return $element['#children'];
}

/**
 * Theme the content of a viewsform_pager element.
 * 
 * @param $element
 *   An associative array containing the properties of the element.
 * @return
 *   A themed HTML string representing the viewsform element.
 *
 * @ingroup themeable
 */
function theme_viewsform_pager_content($element) {
  $output = drupal_render($element['current']);
  $output .= '<div class="pager">'."\n". drupal_render($element) ."</div>\n";
  return $output;
}

function theme_viewsform_row($element) {
  $view = &$element['#view']['view'];
  $row = &$element['#view']['row_plugin'];
  
  return $row->render($element['#view']['row']);
}

function theme_viewsform_field($element) {
  $view = &$element['#view']['view'];
  $field = &$element['#view']['field_handler'];
  
  return $field->theme($element['#view']['row']);
}